<?php

Route::get('/laravelsweetalert/get_flash_message', [
    'middleware' => 'web',
    'as' => 'manager',
    'uses' => 'Maxheckel\LaravelSweetalert\Controllers\FlashAPI@getFlashMessage',
]);