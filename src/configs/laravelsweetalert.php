<?php

return [
    'sessionNames'=>[
        'error'=>'error',
        'success'=>'success',
        'info'=>'info'
    ],
    'requiresSweetAlert'=>false
];