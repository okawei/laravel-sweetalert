<?php

namespace Maxheckel\LaravelSweetalert;

use Illuminate\Support\ServiceProvider;

class LaravelSweetAlertServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/configs/laravelsweetalert.php' => config_path('laravelsweetalert.php'),
        ]);
        $this->loadViewsFrom(__DIR__.'/views', 'laravelsweetalert');
        $this->publishes([
            __DIR__.'/views' => resource_path('views/vendor/laravelsweetalert'),
        ]);
        $this->publishes([
            __DIR__.'/public' => public_path('vendor/laravelsweetalert'),
        ], 'public');
        $this->loadRoutesFrom(__DIR__.'/routes.php');

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
