<?php

namespace Maxheckel\LaravelSweetalert\Controllers;

use Illuminate\Routing\Controller;


class FlashAPI extends Controller  {

    public function getFlashMessage(){
        $message = '';
        if(\Session::has(config('laravelsweetalert.sessionNames.success'))){
            $type = 'success';
            $message = \Session::get('success');
        }
        if(\Session::has(config('laravelsweetalert.sessionNames.error'))){
            $type = 'error';
            $message = \Session::get('error');
        }
        if(\Session::has(config('laravelsweetalert.sessionNames.info'))){
            $type = 'info';
            $message = \Session::get('info');
        }
        if($message == null){
            return [
                'error'=>'No flashed session'
            ];
        }
        return [
            'message'=>$message,
            'type'=>$type
        ];
    }
}