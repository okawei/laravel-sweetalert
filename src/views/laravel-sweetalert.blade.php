@if(config('laravelsweetalert.requiresSweetAlert'))
    <link rel="stylesheet" href="/vendor/laravelsweetalert/components/sweetalert2/dist/sweetalert2.min.css">
@endif
<script>
    //The flash message has to be sent through via ajax so it doesn't show multiple times due to browser caching
    @if(\Session::has(config('laravelsweetalert.sessionNames.success')) || \Session::has(config('laravelsweetalert.sessionNames.error')) || \Session::has(config('laravelsweetalert.sessionNames.info')))

        <?php \Session::reflash();?>
        function httpGetAsync(theUrl, callback)
        {
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.onreadystatechange = function() {
                if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
                    callback(xmlHttp.responseText);
            }
            xmlHttp.open("GET", theUrl, true); // true for asynchronous
            xmlHttp.send(null);
        }
        httpGetAsync( "/laravelsweetalert/get_flash_message", function(data) {
            data = JSON.parse(data);
            if(data.type && data.type == 'success'){
                swal('', data.message, 'success');
            }
            if(data.type && data.type == 'error'){
                swal('', data.message, 'error');
            }
        })
        @endif
</script>

@if(config('laravelsweetalert.requiresSweetAlert'))
    <script src="/vendor/laravelsweetalert/components/sweetalert2/dist/sweetalert2.min.js"></script>
@endif
